import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatSnackBar } from '@angular/material/snack-bar';

export interface IAmplitudePayload {
  api_key: String,
  events: IAmplitudeEvent[]
}

export interface IKeyValue {
  key: string,
  value: string
}

export interface IAmplitudeEvent {
  user_id: String,
  device_id: String,
  event_type: String,
  time?: Date,
  event_props?: Array<IKeyValue>,
  user_props?: Array<IKeyValue>,
  groups_?: Array<IKeyValue>,
  event_properties?: any,
  user_properties?: any,
  groups?: any,
  app_version?: String,
  platform?: String,
  os_name?: String,
  os_version?: String,
  device_brand?: String,
  device_manufacturer?: String,
  device_model?: String,
  carrier?: String,
  country?: String,
  region?: String,
  city?: String,
  dma?: String,
  language?: String,
  price?: Number,
  quantity?: Number,
  revenue?: Number,
  productId?: String,
  revenueType?: String,
  location_lat?: Number,
  location_lng?: Number,
  ip?: String,
  idfa?: String,
  idfv?: String,
  adid?: String,
  android_id?: String,
  event_id?: Number,
  session_id?: Number,
  insert_id?: String
}

export class AmplitudeEvent implements IAmplitudeEvent {
  user_id = "";
  device_id = "";
  event_type = "";
  time?: Date = null;
  event_props? = [];
  user_props? = [];
  groups_? = [];
  event_properties? = {};
  user_properties? = {};
  groups? = {};
  app_version? = "";
  platform? = "";
  os_name? = "";
  os_version? = "";
  device_brand? = "";
  device_manufacturer? = "";
  device_model? = "";
  carrier? = "";
  country? = "";
  region? = "";
  city? = "";
  dma? = "";
  language? = "";
  price?: Number = null;
  quantity?: Number = null;
  revenue?: Number = null;
  productId? = "";
  revenueType? = "";
  location_lat?: Number = null
  location_lng?: Number = null
  ip? = "";
  idfa? = "";
  idfv? = "";
  adid? = "";
  android_id? = "";
  event_id?: Number = null
  session_id?: Number = null;
  insert_id? = "";

  constructor() {
    this.event_props = [];
    this.user_props = [];
    this.groups_ = [];
  }

}

export class AmplitudePayload implements IAmplitudePayload {
  api_key = "44362105e138824178be1f288a20c4bf";
  events = new Array<AmplitudeEvent>();
  constructor() {
    this.events.push(new AmplitudeEvent());
  }
}

export interface IAmplitudeEventDefinition {
  description: String,
  type: String
}

export interface Response {
  code: number,
  events_ingested?: number,
  payload_size_bytes?: number,
  server_upload_time?: number,
  error: string,
  missing_field: string,
  events_with_invalid_fields: Map<String, String>,
  events_with_missing_fields: Map<String, String>,
  eps_threshold: number,
  throttled_devices: Map<String, String>,
  throttled_users: Map<String, String>,
  throttled_events: number
}

@Injectable({
  providedIn: 'root'
})
export class AmplitudeService {

  public amplitudePayload: AmplitudePayload;
  private http2URL = "https://api2.amplitude.com/2/httpapi";
  public amplitudeEventDefinition = new Map<String, IAmplitudeEventDefinition>();

  constructor(private http: HttpClient, private snackBar: MatSnackBar) {
    this.amplitudePayload = new AmplitudePayload();
    this.defineAmplitudeEvent();
  }

  arrayToObject(data: Array<IKeyValue>): any {
    let rawObject = {};
    data.forEach(property => {
      if(property.value.indexOf(';;') > 0) {
        rawObject[property.key] = property.value.split(';;');
      } else {
        rawObject[property.key] = property.value;
      }
    });
    return rawObject;
  }

  prepareData(data: IAmplitudeEvent) : IAmplitudeEvent {
    data.event_properties = this.arrayToObject(data.event_props);
    data.user_properties = this.arrayToObject(data.user_props);
    data.groups = this.arrayToObject(data.groups_);
    return data;
  }

  preparePayload(data: IAmplitudePayload): IAmplitudePayload {
    data.events.forEach(event => {
      this.prepareData(event);
    });
    return data;
  }

  sendAmplitudeEvent() {
    console.log(JSON.stringify(this.amplitudePayload));
    this.http.post<Response>(this.http2URL, this.preparePayload(this.amplitudePayload), {observe: 'response'}).subscribe(response => {
      let message = `STATUS ${response.body.code}: ${response.body.events_ingested} Events, ${response.body.payload_size_bytes} Bytes, ${new Date(response.body.server_upload_time)}`;
      this.snackBar.open(message, 'close');
    }, response => {
      let error = response.error as Response;
      let message = `STATUS ${error.code}: ${error.error}.`;
      if(error.missing_field) {
        message = message + ` Missing Fields: ${error.missing_field}`
      }
      if(error.events_with_invalid_fields) {
        message = message + ` Invalid Fields (field, event index): ${JSON.stringify(error.events_with_invalid_fields)}`
      }
      if(error.events_with_missing_fields) {
        message = message + ` Missing Fields (field, event index): ${JSON.stringify(error.events_with_missing_fields)}`
      }
      if(error.eps_threshold) {
        message = message + ` Events Per Second Threshold: ${error.eps_threshold}`
      }
      if(error.throttled_events) {
        message = message + ` Events Throttled: ${JSON.stringify(error.throttled_events)}`
      }
      if(error.throttled_devices) {
        message = message + ` Devices Throttled (device id, throttle rate): ${JSON.stringify(error.throttled_devices)}`
      }
      if(error.throttled_users) {
        message = message + ` Users Throttled (user id, throttle rate): ${JSON.stringify(error.throttled_users)}`
      }
      this.snackBar.open(message, 'close');
    });
  }

  removeEvent(event: AmplitudeEvent) {
    const index = this.amplitudePayload.events.indexOf(event);
    if (index !== -1) {
      this.amplitudePayload.events.splice(index, 1);
    }
  }

  defineAmplitudeEvent() {
    this.amplitudeEventDefinition.set("user_id", {description: "A readable ID specified by you. Must have a minimum length of 5 characters. Required unless device_id is present.", type: "string"});
    this.amplitudeEventDefinition.set("device_id", {description: "A device-specific identifier, such as the Identifier for Vendor on iOS. Required unless user_id is present. If a device_id is not sent with the event, it will be set to a hashed version of the user_id.", type: "string"});
    this.amplitudeEventDefinition.set("event_type", {description: "A unique identifier for your event. If you would like to make an Identify call, please set 'event_type' to $identify.", type: "string"});
    this.amplitudeEventDefinition.set("time", {description: "The timestamp of the event in milliseconds since epoch. If time is not sent with the event, it will be set to the request upload time.", type: "date"});
    this.amplitudeEventDefinition.set("event_properties", {description: "A dictionary of key-value pairs that represent additional data to be sent along with the event. You can store property values in an array. Date values are transformed into string values.", type: "map"});
    this.amplitudeEventDefinition.set("user_properties", {description: "A dictionary of key-value pairs that represent additional data tied to the user. You can store property values in an array. Date values are transformed into string values.", type: "map"});
    this.amplitudeEventDefinition.set("groups", {description: "This feature is only available to Enterprise customers who have purchased the Accounts add-on. This field adds a dictionary of key-value pairs that represent groups of users to the event as an event-level group. You can only track up to 5 groups.", type: "map"});
    this.amplitudeEventDefinition.set("event_props", {description: "A dictionary of key-value pairs that represent additional data to be sent along with the event. You can store property values in an array. Date values are transformed into string values.", type: "map"});
    this.amplitudeEventDefinition.set("user_props", {description: "A dictionary of key-value pairs that represent additional data tied to the user. You can store property values in an array. Date values are transformed into string values.", type: "map"});
    this.amplitudeEventDefinition.set("groups_", {description: "This feature is only available to Enterprise customers who have purchased the Accounts add-on. This field adds a dictionary of key-value pairs that represent groups of users to the event as an event-level group. You can only track up to 5 groups.", type: "map"});
    this.amplitudeEventDefinition.set("app_version", {description: "The current version of your application.", type: "string"});
    this.amplitudeEventDefinition.set("platform", {description: "Platform of the device.", type: "string"});
    this.amplitudeEventDefinition.set("os_name", {description: "The name of the mobile operating system or browser that the user is using.", type: "string"});
    this.amplitudeEventDefinition.set("os_version", {description: "The version of the mobile operating system or browser the user is using.", type: "string"});
    this.amplitudeEventDefinition.set("device_brand", {description: "The device brand that the user is using.", type: "string"});
    this.amplitudeEventDefinition.set("device_manufacturer", {description: "The device manufacturer that the user is using.", type: "string"});
    this.amplitudeEventDefinition.set("device_model", {description: "The device model that the user is using.", type: "string"});
    this.amplitudeEventDefinition.set("carrier", {description: "The carrier that the user is using.", type: "string"});
    this.amplitudeEventDefinition.set("country", {description: "The current country of the user.", type: "string"});
    this.amplitudeEventDefinition.set("region", {description: "The current region of the user.", type: "string"});
    this.amplitudeEventDefinition.set("city", {description: "The current city of the user.", type: "string"});
    this.amplitudeEventDefinition.set("dma", {description: "The current Designated Market Area of the user.", type: "string"});
    this.amplitudeEventDefinition.set("language", {description: "The language set by the user.", type: "string"});
    this.amplitudeEventDefinition.set("price", {description: "The price of the item purchased. Required for revenue data if the revenue field is not sent. You can use negative values to indicate refunds.", type: "float"});
    this.amplitudeEventDefinition.set("quantity", {description: "The quantity of the item purchased. Defaults to 1 if not specified.", type: "number"});
    this.amplitudeEventDefinition.set("revenue", {description: "revneue = price * quantity. If you send all 3 fields of price, quantity, and revenue, then (price * quantity) will be used as the revenue value. You can use negative values to indicate refunds.", type: "float"});
    this.amplitudeEventDefinition.set("productId", {description: "An identifier for the item purchased. You must send a price and quantity or revenue with this field.", type: "string"});
    this.amplitudeEventDefinition.set("revenueType", {description: "The type of revenue for the item purchased. You must send a price and quantity or revenue with this field.", type: "string"});
    this.amplitudeEventDefinition.set("location_lat", {description: "The current Latitude of the user.", type: "float"});
    this.amplitudeEventDefinition.set("location_lng", {description: "The current Longitude of the user.", type: "float"});
    this.amplitudeEventDefinition.set("ip", {description: "The IP address of the user. Use '$remote' to use your server's IP address. We will use the IP address to reverse lookup a user's location (city, country, region, and DMA). Amplitude has the ability to drop the location and IP address from events once it reaches our servers. You can submit a request to our platform specialist team here to configure this for you.", type: "string"});
    this.amplitudeEventDefinition.set("idfa", {description: "(iOS) Identifier for Advertiser.", type: "string"});
    this.amplitudeEventDefinition.set("idfv", {description: "(iOS) Identifier for Vendor.", type: "string"});
    this.amplitudeEventDefinition.set("adid", {description: "(Android) Google Play Services advertising ID", type: "string"});
    this.amplitudeEventDefinition.set("android_id", {description: "(Android) Android ID (not the advertising ID)", type: "string"});
    this.amplitudeEventDefinition.set("event_id", {description: "(Optional) An incrementing counter to distinguish events with the same user_id and timestamp from each other. We recommend you send an event_id, increasing over time, especially if you expect events to occur simultanenously.", type: "number"});
    this.amplitudeEventDefinition.set("session_id", {description: "(Optional) The start time of the session in milliseconds since epoch (Unix Timestamp), necessary if you want to associate events with a particular system. A session_id of -1 is the same as no session_id specified.", type: "number"});
    this.amplitudeEventDefinition.set("insert_id", {description: "(Optional) A unique identifier for the event. We will deduplicate subsequent events sent with an insert_id we have already seen before within the past 7 days. We recommend generation a UUID or using some combination of device_id, user_id, event_type, event_id, and time.", type: "string"});
  }
}

import { Component, OnInit } from '@angular/core';
import { AmplitudeService, AmplitudeEvent } from '../../amplitude/amplitude.service';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.sass']
})
export class ToolbarComponent implements OnInit {

  constructor(private angularService: AmplitudeService) { }

  ngOnInit(): void {
  }

  addEvent() {
    this.angularService.amplitudePayload.events.push(new AmplitudeEvent())
  }

  sendEvent() {
    this.angularService.sendAmplitudeEvent();
  }

}

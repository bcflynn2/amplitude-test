import { Component, OnInit, Input } from '@angular/core';
import { IKeyValue } from 'src/app/amplitude/amplitude.service';

@Component({
  selector: 'app-property-map',
  templateUrl: './property-map.component.html',
  styleUrls: ['./property-map.component.sass']
})
export class PropertyMapComponent implements OnInit {

  @Input()
  private propertyMap: Array<IKeyValue>;

  constructor() { }

  ngOnInit(): void {
  }

  getPropertyMap() {
    return this.propertyMap;
  }

  addProperty() {
    this.propertyMap.push({key: "", value: ""});
  }

}

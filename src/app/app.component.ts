import { Component } from '@angular/core';
import { AmplitudeService } from './amplitude/amplitude.service'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  title = 'amplitude-test';

  constructor(private amplitudeService: AmplitudeService) {}

  getAmplitudePayload() {
    return this.amplitudeService.amplitudePayload;
  }
}

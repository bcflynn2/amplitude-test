import { Component, OnInit, Input } from '@angular/core';
import { AmplitudeEvent, AmplitudeService, IAmplitudeEvent } from '../../amplitude/amplitude.service'

@Component({
  selector: 'app-event-card',
  templateUrl: './event-card.component.html',
  styleUrls: ['./event-card.component.sass']
})
export class EventCardComponent implements OnInit {

  @Input()
  public event: AmplitudeEvent;

  constructor(private amplitudeService: AmplitudeService) { }

  removeEvent() {
    this.amplitudeService.removeEvent(this.event);
  }

  getOptionProperties() {
    if(this.event) {
      let keys = Object.keys(this.event);
      console.debug(keys);
      return keys;
    } else {
      return [];
    }
  }

  isType(key: String, type: String) {
    return this.getPropertyDetails(key).type === type;
  }

  getPropertyDetails(name: String) {
    return this.amplitudeService.amplitudeEventDefinition.get(name);
  }

  ngOnInit(): void {
  }

}

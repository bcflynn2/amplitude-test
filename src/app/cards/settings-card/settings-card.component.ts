import { Component, OnInit } from '@angular/core';
import { AmplitudeService } from '../../amplitude/amplitude.service';

@Component({
  selector: 'app-settings-card',
  templateUrl: './settings-card.component.html',
  styleUrls: ['./settings-card.component.sass']
})
export class SettingsCardComponent implements OnInit {

  constructor(private amplitudeService: AmplitudeService) {}

  getAmplitudePayload() {
    return this.amplitudeService.amplitudePayload;
  }

  ngOnInit(): void {
  }

}

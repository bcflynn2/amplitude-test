import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatDatepickerModule,  } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SettingsCardComponent } from './cards/settings-card/settings-card.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';
import { EventCardComponent } from './cards/event-card/event-card.component';
import { ToolbarComponent } from './components/toolbar/toolbar.component';
import { AmplitudeService } from './amplitude/amplitude.service'
import { HttpClientModule } from '@angular/common/http';
import { PropertyMapComponent } from './components/property-map/property-map.component';


@NgModule({
  declarations: [
    AppComponent,
    SettingsCardComponent,
    EventCardComponent,
    ToolbarComponent,
    PropertyMapComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    MatToolbarModule,
    MatExpansionModule,
    MatSnackBarModule,
    MatTooltipModule,
    MatDatepickerModule,
    MatNativeDateModule,
    FlexLayoutModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [AmplitudeService],
  bootstrap: [AppComponent]
})
export class AppModule { }
